# portfolio

Hi! Welcome to my portfolio! My Name is Lisa, I am a fullstack-dev in training at Coders.Bay Vienna.
Down below you can find some of the Projects I have worked on until now! 


## Projects

* [Fire Scamblem](https://gitlab.com/LiVora/java-game-project)



Our final project for the second semester at Coders.Bay, which was created in collaboration with Laura Huber-Lechner. 
It's a game, inspired by early Fire Emblem Titles.
We were challenged to create the game in the windows terminal with no GUI, using the minimax/negamax algorithm for the
enemy-army. 


* [Management Platform](https://gitlab.com/tipali1/fullstackproject)


The first semester-project we worked on at Coders.Bay, I worked with Christina Stockinger and Patrick Joszt.
It is a fullstack application that helped us to get to know the basics of programming in Java.
I mostly worked on the front-end aspect of the application.